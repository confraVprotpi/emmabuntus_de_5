#! /bin/bash


# emmabuntus_background.sh --
#
#   This file permits to configure early the background.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################

img_ref=/usr/share/xfce4/backdrops/Emmabuntus_Ice_login.jpg

img=$(grep ^background ./etc/lightdm/lightdm-gtk-greeter.conf | cut -f2 -d= | sed 's/\ //' | cut -f2 -d:)


if [[ ${img} == "" ]] ; then
    echo "Background not defined use default"
    (feh --bg-fill $img_ref)&
elif test -f ${img} ; then
    echo "Display Background"
    (feh --bg-fill $img)&
else
    echo "Background not found"
fi
