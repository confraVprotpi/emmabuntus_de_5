#! /bin/bash

# dwservice_wrapper.sh --
#
#   This file permits to launch DWService for the Emmabuntüs Distribs
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear


nom_distribution="Emmabuntüs"

nom_logiciel_affichage="DWService"
nom_logiciel_exe=dwagent.sh
dir_install=/opt/DWService

URL_DWSERVICE="https://www.dwservice.net/"


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


if [[ -f /usr/bin/emmabuntus_found_theme.sh ]] ; then
    source /usr/bin/emmabuntus_found_theme.sh
    highlight_color=$(FOUND_HIGHLIGHT_COLOR)
else
    highlight_color=red
fi

if [[ ${LANG} == fr* ]] ; then

message="\n\
$(eval_gettext 'Solution libre de contrôle d\047un ordinateur à distance'). $(eval_gettext 'Sélectionnez l\047un des deux modes d\047utilisation :')\n\
\n\
\<span color=\'${highlight_color}\'>- $(eval_gettext 'Autoriser le contrôle') :\</span> $(eval_gettext 'Permet à un autre utilisateur de prendre le contrôle à distance de votre ordinateur.')\n\
$(eval_gettext 'Pour cela, validez le mode \042Exécuter\042 dans la fenêtre qui va s\047ouvrir, puis cliquez sur \042Suivant\042.')\n\
$(eval_gettext 'Transmettez ensuite les codes de connexion de la deuxième fenêtre à la personne qui désire prendre le contrôle de votre ordinateur,')\n\
$(eval_gettext 'et laissez cette fenêtre ouverte durant toute la durée de la prise de contrôle à distance.')\n\
\n\
\<span color=\'${highlight_color}\'>- $(eval_gettext 'Contrôler un ordinateur') :\</span> $(eval_gettext 'Permet de prendre le contrôle à distance d\047un autre ordinateur.')\n\
$(eval_gettext 'Pour cela, sur la page Internet de \<b>DWService\</b>, renseignez les codes fournis par la personne')\n\
$(eval_gettext 'qui désire que vous contrôliez son ordinateur,')\n\
$(eval_gettext 'dans le cadre \042Identifiant\042, puis cliquez sur \042Se connecter\042.')\n"

bouton_annuler="Annuler"
bouton_autoriser="Autoriser le contrôle"
bouton_controler="Contrôler un ordinateur"

else

message="\n\
$(eval_gettext 'Free solution for computer remote control'). $(eval_gettext 'Select one of the two modes of use :')\n\
\n\
\<span color=\'${highlight_color}\'>- $(eval_gettext 'Authorize the control') :\</span> $(eval_gettext 'Allows another user to remotely take the control of your computer.')\n\
$(eval_gettext 'To pick this option, select the \042Run\042 mode in the next window, then click on \042Next\042.')\n\
$(eval_gettext 'Then send the connexion codes of the second window to the person wants to take the control of your computer,')\n\
$(eval_gettext 'and leave this window open during all the time of the remote control session.')\n\
\n\
\<span color=\'${highlight_color}\'>- $(eval_gettext 'Control a computer') :\</span> $(eval_gettext 'Allows you to remotely control another computer.')\n\
$(eval_gettext 'To do this, go to the \<b>DWService\</b> internet page, enter the codes sent by the person')\n\
$(eval_gettext 'who wants you to take control of his or of her computer,')\n\
$(eval_gettext 'in the \042Login\042 frame, then click on \042Sign in\042.')\n"

bouton_annuler="Cancel"
bouton_autoriser="Authorize the control"
bouton_controler="Control a computer"

fi


export WINDOW_DIALOG='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message'" | sed "s%\\\%%g"</input>
</text>

<hbox spacing="10" space-expand="false" space-fill="false">

<button>
<input file icon="gtk-no"></input>
<label>"'${bouton_annuler}'"</label>
<action>exit:exit</action>
</button>


<button>
<label>"'${bouton_autoriser}'"</label>
<input file stock="gtk-execute"></input>
<action>exit:AUTORISER</action>
</button>

<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>"'${bouton_controler}'"</label>
<input file stock="gtk-connect"></input>
<action>exit:CONTROLER</action>
</button>

</hbox>

</vbox>
</window>'

MENU_DIALOG_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

eval ${MENU_DIALOG_DIALOG}


if [[ ${EXIT} == "AUTORISER" ]] ; then

    ${dir_install}/${nom_logiciel_exe}

elif [[ ${EXIT} == "CONTROLER" ]] ; then

    xdg-open ${URL_DWSERVICE}

fi

